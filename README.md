# php-extended/php-basic-console-logger
A very basic psr-3 compliant logger to log activity in the console.

![coverage](https://gitlab.com/php-extended/php-basic-console-logger/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-basic-console-logger/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-basic-console-logger ^8`


## Basic Usage

This library gives a very basic psr-3 logger. Use it as described in the docs
of the psr-3 : [https://www.php-fig.org/psr/psr-3/](https://www.php-fig.org/psr/psr-3/)

```php

use PhpExtended\Logger\BasicConsoleLogger;

$logger = new BasicConsoleLogger();

$logger->error('Nah.');	// echo YYYY-MM-DD HH:mm:SS [  ERROR  ] Nah.\n

```


## License

MIT (See [license file](LICENSE)).
