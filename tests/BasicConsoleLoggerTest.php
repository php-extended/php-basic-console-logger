<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-basic-console-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Logger\BasicConsoleLogger;
use PHPUnit\Framework\TestCase;

/**
 * BasicConsoleLoggerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Logger\BasicConsoleLogger
 *
 * @internal
 *
 * @small
 */
class BasicConsoleLoggerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BasicConsoleLogger
	 */
	protected BasicConsoleLogger $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testEmergency() : void
	{
		$this->_object->emergency('Emergency {msg}', ['msg' => 'Message']);
		$this->assertTrue(true);
	}
	
	public function testAlert() : void
	{
		$this->_object->alert('Alert {msg}', ['msg' => 'Message']);
		$this->assertTrue(true);
	}
	
	public function testCritical() : void
	{
		$this->_object->critical('Critical {msg}', ['msg' => 'Message']);
		$this->assertTrue(true);
	}
	
	public function testError() : void
	{
		$this->_object->error('Error {msg}', ['msg' => 'Message']);
		$this->assertTrue(true);
	}
	
	public function testWarning() : void
	{
		$this->_object->warning('Warning {msg}', ['msg' => 'Message']);
		$this->assertTrue(true);
	}
	
	public function testNotice() : void
	{
		$this->_object->notice('Notice {msg}', ['msg' => 'Message']);
		$this->assertTrue(true);
	}
	
	public function testInfo() : void
	{
		$this->_object->info('Info {msg}', ['msg' => 'Message']);
		$this->assertTrue(true);
	}
	
	public function testDebug() : void
	{
		$this->_object->debug('Debug {msg}', ['msg' => 'Message']);
		$this->assertTrue(true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BasicConsoleLogger(4);
	}
	
}
